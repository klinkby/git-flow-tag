param (
    [Parameter(Position=0,Mandatory=$True,HelpMessage="Enter semver to create (e.g. '1.0.0.99')")]
    [ValidatePattern({^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$})]
    $version
)

# branch master
git checkout master
git pull
git checkout -b release/$version
# merge in develop
git merge -X theirs -m "release $version" develop
# commit, push to origin
git add .
git commit -m "release $version"
git push --set-upstream origin release/$version
# merge to master
git checkout master
git merge -X theirs -m "release $version" release/$version
# commit, force push to origin
git add .
git commit -m "release $version"
git push --set-upstream origin master
git tag -a $version -m $version
git push origin --force --tags
# back to develop
git checkout develop